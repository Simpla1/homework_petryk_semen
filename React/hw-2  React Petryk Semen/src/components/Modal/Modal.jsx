import React, { PureComponent } from 'react';
import Button from "../Button/Index";
import modalStyle from "./modalStyle.scss";



class Modal extends PureComponent {

  render() {
    let { name, text, onClick, toggleAdded, vendorСode } = this.props

    return (
      <div className="modalOverlay" onClick={onClick}>
            <div className="modalwindow">
                <div className="modalwindow__header">
                    <div className="modalwindow__title modal-close">{name}</div>
                    <span className="modal-close__cross">&#xD7;</span>
                </div>
                <div className="modalwindow__body">
                    {text} 
                </div>
                <div className="modalwindow__footer">
                    <Button onClick={() => toggleAdded(vendorСode)} text="Accept" className="btn btn-success"/>
                    <Button text="Cancel" className="btn btn-danger"/>
                </div>
            </div>
        </div>

    )
  }
}

export default Modal;

