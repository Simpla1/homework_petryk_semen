import React, { PureComponent } from 'react';
import Button from '../Button/Index';


class ButtonList extends PureComponent {
    render() {
        const { modalFirstOpen } = this.props     
        return (
            <>
                <Button
                 onClick={modalFirstOpen}
                    />
                 
            </>
        )
    }
}
export default ButtonList;