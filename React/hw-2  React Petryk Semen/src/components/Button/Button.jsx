import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import "./style.scss";


export default class Index extends PureComponent {
    
    render() {
        const {onClick} = this.props
        return <button
        onClick={onClick}
        className={this.props.className}
        text={this.props.text}
        >
       {this.props.text}
        </button>;
    }
}

Index.protoType = {
    text: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired
}

Index.defaultProps = {
    text: 'ADD TO CARD',
    className: "btn btn-primary btn-sm"

}