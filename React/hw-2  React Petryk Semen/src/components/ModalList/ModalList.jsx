import React, { PureComponent } from 'react';
import Modal from '../Modal/Index';
import ButtonList from '../ButtonList/Index';
import './style.scss';


class ModalList extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            isOpenFirst: false
        };
    }
    modalFirstOpen = (event) => {

        this.setState(state => {
            return { isOpenFirst: true }
        })
    }
    modalFirstClose = (event) => {
        this.setState(state => {
            return { isOpenFirst: !state.isOpenFirst }

        })
    }
 render() {
        const modalContent1 = " Вы хотитете добавить медикамент в корзину?" 
        const {toggleAdded, vendorСode} = this.props    
        return (
            <>
            <div className="containerBtn">
                <ButtonList
                    modalFirstOpen={this.modalFirstOpen}                           
                />
            </div>
            <div>
                    {this.state.isOpenFirst &&
                    <Modal
                    vendorСode={vendorСode}
                    toggleAdded={toggleAdded}
                    name="Добавление товара в корзину"
                        text={modalContent1}
                     onClick={this.modalFirstClose}
                     />
                    }
            </div>
        </>

        )
    }
}

export default ModalList;