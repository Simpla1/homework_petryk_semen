import React, { useState, useEffect } from 'react';
import axios from 'axios';
import ProductList from '../../components/ProductList/index';



const Home = () => {

    const [cards, setCards] = useState([]);
    
    useEffect(() => {
        axios("medical.json")
            .then(res => {
                const normalizeArray = normalizeData(res.data);
                updateCards(normalizeArray)
            })
    }, [])

    const normalizeData = (data) => {
        return data.map(cardPlus => {
            const favorites = JSON.parse(localStorage.getItem('favorites')) || []
            const added = JSON.parse(localStorage.getItem('added')) || []
           cardPlus.isInfavorite = favorites.includes(cardPlus.vendorСode)
           cardPlus.added = added.includes(cardPlus.vendorСode)
           return cardPlus
       })
   }
    const updateCards = (cardPlus) => {
    setCards(cardPlus)
  }

   const favoriteLocalStorage = (vendorСode) => {
      let array = JSON.parse(localStorage.getItem('favorites')) || []
     array = (array.includes(vendorСode) ? array.filter(el => el !== vendorСode) : array.concat(vendorСode))
      const favorites= JSON.stringify(array)
      localStorage.setItem('favorites', favorites)
  }

    const toggleFavorite = (vendorСode) => {
    const newArray =  cards.map(el => {
        if (el.vendorСode=== vendorСode) {
            el.isInfavorite = !el.isInfavorite      
        }
        return el
    })

    favoriteLocalStorage(vendorСode)
    setCards(newArray)
  }



  const addedLocalStorage = (vendorСode) => {
    let array = JSON.parse(localStorage.getItem('added')) || []
   array = (array.includes(vendorСode) ? array.filter(el => el !== vendorСode) : array.concat(vendorСode))
    const added= JSON.stringify(array)
    localStorage.setItem('added', added)
}

  
  const toggleAdded = (vendorСode) => {
    const newArray =  cards.map(el => {
        if (el.vendorСode=== vendorСode) {
            el.added = !el.added      
        }
        return el
    })

    addedLocalStorage(vendorСode)
    setCards(newArray)
  }
  
    return (
        <div>
            <ProductList 
            toggleAdded={toggleAdded}
            toggleFavorite={toggleFavorite}
            cards={cards}   
             />
        </div>
    )
}

export default Home;



