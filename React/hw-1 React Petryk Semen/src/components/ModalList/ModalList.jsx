import React, { PureComponent } from 'react';
import Modal from '../Modal/Index';
import ButtonList from '../ButtonList/Index';
import './style.scss';


class ModalList extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            isOpenFirst: false,
            isOpenSecond: false
        };
    }

    modalFirstOpen = (event) => {

        this.setState(state => {
            return { isOpenFirst: true }
        })
    }

    modalSecondOpen = (event) => {
           this.setState(state => {
            return { isOpenSecond: true }
        })
    }

    modalFirstClose = (event) => {
        this.setState(state => {
            return { isOpenFirst: !state.isOpenFirst }

        })
    }

    modalSecondClose = (event) => {
        this.setState(state => {
            return { isOpenSecond: !state.isOpenSecond }

        })
    }

    render() {
        const modalContent1 = " Модальное окно в графическом интерфейсе пользователя — окно, которое блокирует работу пользователя с родительским приложением до тех пор, пока пользователь это окно не закроет. Модальными преимущественно реализованы диалоговые окна. "
        const modalContent2 = "A modal window in a graphical user interface is a window that which blocks user interaction with the parent application as long as until the user closes this window. Modal predominantlyimplemented dialo." 
        
        return (
            <>
            <div className="containerBtn">
                <ButtonList
                    modalFirstOpen={this.modalFirstOpen}
                    modalSecondOpen={this.modalSecondOpen}           
                />
            </div>
            <div>
                    {this.state.isOpenFirst &&
                    <Modal
                    name="Модальное окно №1"
                        text={modalContent1}
                     onClick={this.modalFirstClose}
                     />
                    }
            </div>
            <div>         
                    {this.state.isOpenSecond &&
                    <Modal
                        name="Modal window №2"
                        text={modalContent2}
                        onClick={this.modalSecondClose}
                    />
                    }
            </div> 
             </>

        )
    }
}

export default ModalList;