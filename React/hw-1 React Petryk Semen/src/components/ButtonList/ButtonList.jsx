import React, { PureComponent } from 'react';
import Button from '../Button/Index';


class ButtonList extends PureComponent {
    render() {
        const { modalFirstOpen, modalSecondOpen } = this.props     
        return (
            <>
                <Button
                 onClick={modalFirstOpen}
                    />
                 <Button
                  onClick={modalSecondOpen}
                  className="btn btn-danger btn-lg"
                   text="Open second modal"
                    />
            </>
        )
    }
}
export default ButtonList;