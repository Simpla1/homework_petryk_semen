import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';


export default class Index extends PureComponent {
    
    render() {
        const {onClick} = this.props
        return <button
        onClick={onClick}
        className={this.props.className}
        >
       {this.props.text}
        </button>;
    }
}

Index.protoType = {
    text: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired
}

Index.defaultProps = {
    text: 'Open first modal',
    className: "btn btn-primary btn-lg"

}