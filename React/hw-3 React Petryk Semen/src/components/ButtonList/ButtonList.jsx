import React, { PureComponent } from 'react';
import Button from '../Button/Index';


class ButtonList extends PureComponent {
    render() {
        const { modalFirstOpen, added } = this.props     
        return (
            <>
                <Button
                added={added}
                 onClick={modalFirstOpen}
                 text={added ? "DELETE CARD" : "ADD TO CART"}
                    /> 
            </>
        )
    }
}
export default ButtonList;