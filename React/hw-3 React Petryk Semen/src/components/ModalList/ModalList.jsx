import React, { useState } from 'react';
import Modal from '../Modal/Index';
import ButtonList from '../ButtonList/Index';
import './style.scss';



const ModalList = (props) => {

    const [isOpenFirst, setIsOpenFirst] = useState(false);

    const modalFirstOpen = () => {
        setIsOpenFirst(true)
    }

    const modalFirstClose = () => {   
        setIsOpenFirst(false)

    }

    const modalContent1 = " Вы хотитете добавить медикамент в корзину?"
    const { toggleAdded, vendorСode, added } = props
    return (
        <>
            <div className="containerBtn">
                <ButtonList
                    added={added}
                    modalFirstOpen={modalFirstOpen}
                />
                
            </div>
            <div>
                {isOpenFirst &&   
                    <Modal    
                        vendorСode={vendorСode}
                        toggleAdded={toggleAdded}
                        name="Добавление товара в корзину"
                        text={modalContent1}
                        modalFirstClose={modalFirstClose}
                    />
                }
            </div>
        </>
    )
}

export default ModalList

