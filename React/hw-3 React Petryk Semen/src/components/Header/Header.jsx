import React from 'react';
import './style.scss';
import logo from './logo.png'
import Sidebar from '../../components/Sidebar/Sidebar';

const Header = () => {
    return (
      <header>
       <nav className="navbar color"> 
        <div>
            <img img src={logo} alt={"logo"} className="logo" />
        </div>
        <div className="navbar__sidebar-menu">
        <Sidebar />
        </div>
      </nav>
    </header>
    )
}

export default Header
