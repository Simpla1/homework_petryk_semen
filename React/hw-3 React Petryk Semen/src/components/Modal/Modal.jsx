import React from 'react';
import Button from "../Button/Index";
import "./style.scss";


const Modal = (props) => {

  let { name, text, modalFirstClose, toggleAdded, vendorСode } = props

  return (
    <div className="modalOverlay" onClick={modalFirstClose}>
      <div className="modalwindow">
        <div className="modalwindow__header">
          <div className="modalwindow__title modal-close">{name}</div>
          <span className="modal-close__cross">&#xD7;</span>
        </div>
        <div className="modalwindow__body">
          {text}
        </div>
        <div className="modalwindow__footer">
          <Button onClick={() => toggleAdded(vendorСode)} text="Accept" className="btn btn-success" />
          <Button text="Cancel" className="btn btn-danger" />
        </div>
      </div>
    </div>
  )
}

export default Modal

