import React from 'react';
import './cart.scss';

const Cart = (props) => {
    const { cards, toggleAdded } = props
    return (
        <div>
         <ul className="albums container">
          <div className="albums__header-container">
                <h2 className="albums__title">CART</h2>
            </div>
          <div className="albums__title-container" >
            {cards
                .filter(el => el.added)
                .map(el => 
                <div className="album__content">

                
                    {el.title}
                    <button onClick={() => toggleAdded(el.vendorСode)}>удалить</button>
                </div>
                )
            }
         </div>
       </ul>
     </div>
    )
}

export default Cart
